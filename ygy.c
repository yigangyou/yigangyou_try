
/* This file is for cli and snmp to upgrade smm image(flash1/flash2), only valid on smm board */
#include "mac_rpc.h"
#include "dcts.h"   //  fjslafjsakfgsagjsalkgjlokajdlka

#include "immSetIf.h"
#include "immGetIf.h"
#include "immIf.h"
#include "cliShell.h"
#include "immIf2.h"
#include "trace.h"
#include "bpi_rpc.h"
#include "chassis.h"
#include "crl_rpc_cert_with_file_def.h"
#include "crl_rpc_cert_with_file_client.h"
#include "ddm_rpc_smm_client.h"
#include "casa_math.h"

#include "interface_cfg.h"
#include "bpi_cfg.h"
#include "bg_sg_cfg.h"
#include "smm_cfg.h"
#include "smmutil.h"
#include "rf_cfg.h"
#include "lcutil.h"
#include "sec_cfg.h"
#include "macaddr.h"
#include "dctsutil.h"
#include "show_cm.h"
#include "cli_rpc.h"
#include "pon_rpc_client.h"
#include "pkt_gate.h"


extern void DctsCtrShowCmPrivacyDetialByMac(FILE *fp, ddm_rpc_privacy_verbose_cm_t* privacy_verbose_cm, int cm_cnt);
extern void DctsCtrShowvCmPrivacyDetialByMac(FILE *fp, ddm_rpc_privacy_verbose_cm_t* p_cm, int cm_cnt, ddm_mac_cm_bak_data_t cm_static);

//shared-secret [(0 | 7)] <string> [extend]
int shared_secret_callback(int numToken, char *cliArgv[])
{
	db_shared_secret_t key;
	db_shared_secret_key_t s_key;
	int cli_keytype = SharedSecretKeyType_unEncrypt;
	u_int8_t keystr[512] = {0};
	size_t len = 0;
	int shared_secret_type = NORMAL_SHARED_SECRET;
	int is_ext = 0;

	u_int8_t cmts_mic_enable = DO_NOT_CHECK_CMTS_MIC;
	memset(&key, 0, sizeof(key));

	if (!infoCenter.isNoCmd)
	{
		if (numToken == 4 || (numToken == 3
			&& (strcmp(cliArgv[1], "0") != 0 && strcmp(cliArgv[1], "7") != 0)
			&& strcmp(cliArgv[2], "extend") == 0)   )
		{
			shared_secret_type = EXT_SHARED_SECRET;
			is_ext = 1;
		}
		char *str;
		cmts_mic_enable = CHECK_CMTS_MIC;
		if (numToken == 3 + is_ext)
		{
			cli_keytype = atoi(cliArgv[1]);
			len = strlen(cliArgv[2]);
			char val[5] = {0};
			str = cliArgv[2];
			if (cli_keytype == SharedSecretKeyType_Encrypt)
			{
				strcpy(val, "0x00");
				if (len % 2 != 0)
				{
					printf("wrong shared-secret encryption key\n");
					return ERROR;
				}

				if (len / 2 > sizeof(key.key))
				{
					printf("shared-secret encryption key is too long\n");
					return ERROR;
				}

				int i;
				for ( i = 0; i < len; i += 2)
				{
					memcpy(val + 2, str + i, 2);
					keystr[i/2] = (u_int8_t) aToU(val);
				}

				key.keytype = cli_keytype;
				key.keylen = len / 2;
				memcpy(key.key, keystr, key.keylen);
			}
		}
		else
		{
			str = cliArgv[1];
		}

		if (cli_keytype != SharedSecretKeyType_Encrypt)
		{
			// decrypt key
			key.keytype = cli_keytype;
			key.keylen = strlen(str);
			memcpy(key.key, str, key.keylen);
		}

	}
	else 	//no cmd
	{
		if (numToken == 3)
		{
			shared_secret_type = EXT_SHARED_SECRET;
			is_ext = 1;
		}
	}

	key.keytype = cli_keytype;
	key.cmts_mic_enable = cmts_mic_enable;

	s_key.secondary_index = 0; // not a secondary key, so set it's index == 0
	if(shared_secret_type == NORMAL_SHARED_SECRET)
	{
		s_key.key_type = DB_SHARED_SECRET_KEY;
	}
	else
	{
		s_key.key_type = DB_EXTEND_SHARED_SECRET_KEY;
	}

	s_key.mac_id = 0;
	s_key.reserved = 0;
	if(MODE_EQ(infoCenter.curMode, M_CONF_IF_MAC))
	{
		s_key.mac_id = infoCenter.intfId;
	}
	return cfg_cb_shared_key(0, Cfg_act_update, &s_key, &key, NULL, NULL);
}


//shared-secondary-secret (1-2) [0|7] <string> [extend]
int shared_secondary_secret_callback(int numToken, char *cliArgv[])
{
	db_shared_secret_t key;
	db_shared_secret_key_t s_key;
	int secondary_key_index = 0;
	int cli_keytype = SharedSecretKeyType_unEncrypt;
	u_int8_t keystr[512] = {0};
	size_t len = 0;
	int shared_secret_type = NORMAL_SHARED_SECRET;
	int is_ext = 0;

	u_int8_t cmts_mic_enable = DO_NOT_CHECK_CMTS_MIC;
	memset(&key, 0, sizeof(key));

	if (!infoCenter.isNoCmd)
	{
		if (numToken == 5 || (numToken == 4
			&& (strcmp(cliArgv[2], "0") != 0 && strcmp(cliArgv[2], "7") != 0)
			&& strcmp(cliArgv[3], "extend") == 0)   )
		{
			shared_secret_type = EXT_SHARED_SECRET;
			is_ext = 1;
		}
		char *str;
		cmts_mic_enable = CHECK_CMTS_MIC;
		secondary_key_index = atoi(cliArgv[1]);
		if (numToken == 4 + is_ext)
		{
			cli_keytype = atoi(cliArgv[2]);
			len = strlen(cliArgv[3]);
			char val[5] = {0};
			str = cliArgv[3];
			if (cli_keytype == SharedSecretKeyType_Encrypt)
			{
				strcpy(val, "0x00");
				if (len % 2 != 0)
				{
					printf("wrong shared-secret encryption key\n");
					return ERROR;
				}

				if (len / 2 > sizeof(key.key))
				{
					printf("shared-secret encryption key is too long\n");
					return ERROR;
				}

				int i;
				for ( i = 0; i < len; i += 2)
				{
					memcpy(val + 2, str + i, 2);
					keystr[i/2] = (u_int8_t) aToU(val);
				}
				key.keytype = cli_keytype;
				key.keylen = len / 2;
				memcpy(key.key, keystr, key.keylen);
			}
		}
		else
		{
			str = cliArgv[2];
		}

		if (cli_keytype != SharedSecretKeyType_Encrypt)
		{
			key.keytype = cli_keytype;
			key.keylen = strlen(str);
			memcpy(key.key, str, key.keylen);
		}
	}
	else 	//no cmd
	{
		//no shared-secondary-secret (1-2) [extend]
		if (numToken == 4)
		{
			shared_secret_type = EXT_SHARED_SECRET;
			is_ext = 1;
		}
		secondary_key_index = atoi(cliArgv[2]);
	}

	if(secondary_key_index > MAX_SHARED_SECONDARY_SECRET_NUM)
	{
		printf("shared-secondary-secret index error\n");
		return ERROR;
	}

	key.keytype = cli_keytype;
	key.cmts_mic_enable = cmts_mic_enable;

	s_key.secondary_index = secondary_key_index; // not a secondary key, so set it's index == 0
	if(shared_secret_type == NORMAL_SHARED_SECRET)
	{
		s_key.key_type = DB_SHARED_SECRET_KEY;
	}
	else
	{
		s_key.key_type = DB_EXTEND_SHARED_SECRET_KEY;
	}

	s_key.mac_id = 0;
	s_key.reserved = 0;
	if(MODE_EQ(infoCenter.curMode, M_CONF_IF_MAC))
	{
		s_key.mac_id = infoCenter.intfId;
	}

	return cfg_cb_shared_key(0, Cfg_act_update, &s_key, &key, NULL, NULL);
}

int cable_privacy_des40_callback(int numToken, char *cliArgv[])  //20971 by xmliu
{
	db_sec_global_t res_data;

	if (db_sec_global_get(&res_data)!=CDB_OK)
	{
		printf("get sec config from database failed!\n");
		return -1;
	}

	if (infoCenter.isNoCmd)
	{
		res_data.bpi_des40_enforce = 0;
	}
	else
	{
		res_data.bpi_des40_enforce = 1;
	}

	return db_sec_des_40_enforce_set(0,Cfg_act_update,NULL,&res_data,NULL,NULL);
}

int cable_privacy_test_edrca_callback(int numToken, char *cliArgv[])
{
	crl_io_data_t result_buff;
	crl_base_rpc_out *res =
		crl_rpc_switch_real_to_test_root_cert_clt((infoCenter.isNoCmd)? FALSE: TRUE, &result_buff);

	if (!res || res->ret_value != 0)
	{
		fprintf(stderr, "command failed!\n");
		return ERROR;
	}
	return OK;
}
